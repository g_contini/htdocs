<!-- include il file config.php -->
<?php require_once('config.php') ?>
<?php require_once(ROOT_PATH . '/includes/public_functions.php') ?>

<!--Ritorna i post che risultano pubblicati---->
<?php $posts = getPublishedPosts(); ?>

<?php require_once(ROOT_PATH . '/includes/head_section.php') ?>

<link rel="stylesheet" href="/static/css/about.css">

<script>
$(window).scroll(function(){
	$('nav').toggleClass('scrolled', $(this).scrollTop() > 360);
});
</script>

<script>
    $(window).scroll(function(){
        if($(this).scrollTop() > 360){
            $('.abc').addClass('ncol');}
        else{
            $('.abc').removeClass('ncol');}
    });
</script>

</head>

<body>
<?php $page = 'about';
include(ROOT_PATH . '/includes/navbar.php') ?>
<section class="about pt-5 mt-5">
    <div class="container  p-2 card border-0">
        <div class="row">

            <div class="col-md-6 ">
                <div class="card border-0 p-2 ">
                    <div class="card-title border-bottom rounded cardStyleTitle"><h3>Il nostro team</h3></div>
                    <div class="card-body cardStyleBody rounded">
                        <ul>
                            <li><h5>Responsabile del progetto:</h5></li>
                            <i>Stanislao Fidanza</i>
                            <li><h5>Responsabile dei test:</h5></li>
                            <i>Paolo Tamborino</i>
                            <li><h5>Responsabile Gestione Configurazione:</h5></li>
                            <i>Giorgio Contini</i>
                            <li><h5>Responsabile Controllo Qualità:</h5></li>
                            <i>Giuseppe Pio Damato</i>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-6 ">
                <div class="card border-0 p-2 ">
                    <div class="card-title border-bottom rounded cardStyleTitle"><h3>I nostri obiettivi</h3></div>
                    <div class="card-body cardStyleBody rounded">
                        <div class="lead">Il progetto nasce con l'obiettivo principale di poter esprimere il nostro pensiero in merito ai più svariati
                            argomenti.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 p-2 ">
                    <div class="card-title border-bottom rounded cardStyleTitle"><h3>Sviluppi futuri</h3></div>
                    <div class="card-body cardStyleBody rounded">
                        <div class="lead">
                            Tra i prossimi sviluppi ci sarà, per i "guest", la possibilità di interagire con gli articoli tramite "like" e commenti.
                            Verrà anche data la possibilità di proporre interi articoli che noi amministratori, dopo aver controllato la conformità delle
                            proposte, provvederemo a pubblicare.
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


</section>

<?php include(ROOT_PATH . '/includes/footer.php') ?>


















