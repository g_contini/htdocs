<?php include('config.php'); ?>
<?php include('includes/public_functions.php'); ?>
<?php
// Estrae il post dal database
if (isset($_GET['post-slug'])) {
	$post = getPost($_GET['post-slug']);
}
?>
<?php include('includes/head_section.php'); ?>
<title> <?php echo $post['title'] ?> </title>
</head>

<body>

	<!-- Navbar -->
	<?php include(ROOT_PATH . '/includes/navbar.php'); ?>
	<!-- // Navbar -->

	<div class="container">

		<div class="row mybg">
			<h1 class="post-title " style="margin-bottom: 100px "><?php echo $post['title'] ?></h1>
		</div>
		<div class="row" style="justify-content: center">
			<img src="<?php echo BASE_URL . '/static/images/' . $post['image']; ?>" alt="">
		</div>

		<div class="row" style="margin-top: 100px">

			<div class="col">
				<p>
					<!-- Converte l'entità html <p> nei relativi caratteri -->
					<?php echo html_entity_decode($post['body']); ?>
				</p>
			</div>
		</div>

	</div>


	<!-- // content -->

	<?php include(ROOT_PATH . '/includes/footer.php'); ?>