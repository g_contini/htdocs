<?php include('../config.php'); ?>
<?php include(ROOT_PATH . '/admin/includes/admin_functions.php'); ?>
<?php
// Prendi glia admin dal db
$admins = getAdminUsers();
$roles = ['Admin', 'Author'];
?>
<?php include(ROOT_PATH . '/includes/head_section.php'); ?>
<title>Admin | Manage s</title>
</head>

<body>
	<!--Navbar admin -->
	<?php $page='impostazioni'; include(ROOT_PATH . '/admin/navbar_admin.php') ?>


	<div class="container content mt-5 pt-5">

		<div class="action">
			<h1 class="page-title">Modifica Admin</h1>

			<form method="post" action="<?php echo BASE_URL . 'admin/informazioni.php'; ?>">

				<!-- Include i possibili errori -->
				<?php include(ROOT_PATH . '/includes/errors.php') ?>

				<?php if ($isEditingUser === true) : ?>
					<input type="hidden" name="admin_id" value="<?php echo $admin_id; ?>">
				<?php endif ?>

				<div class="form-group row">
					<div class="col-sm-10 ">
						<input type="text" name="username" value="<?php echo $username; ?>" placeholder="Username">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10">
						<input type="email" name="email" value="<?php echo $email ?>" placeholder="Email">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10">
						<input type="password" name="password" placeholder="Password">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10">
						<input type="password" name="passwordConfirmation" placeholder="Conferma Password">
					</div>
				</div>



				<?php if ($isEditingUser === true) : ?>
					<button type="submit" class="btn" name="update_admin">UPDATE</button>

				<?php endif ?>
			</form>
		</div>
	</div>



	<div class="container mt-5">
		<div class="table-responsive">
			<?php include(ROOT_PATH . '/includes/messages.php') ?>

			<?php if (empty($admins)) : ?>
				<h1>No admins in the database.</h1>
			<?php else : ?>
				<table class="table table-editable">
					<thead>
						<tr>
							<th>N</th>
							<th>Admin</th>
							<th colspan="2">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($admins as $key => $admin) : ?>
							<tr>
								<td><?php echo $key + 1; ?></td>
								<td>
									<?php echo $admin['username']; ?>, &nbsp;
									<?php echo $admin['email']; ?>
								</td>
								<td>
									<a style="color:black" href="impostazioni.php?edit-admin=<?php echo $admin['id'] ?>">
										<i class="fas fa-edit"></i>
									</a>
								</td>
								<td>
									<a class="fa fa-trash btn delete" href="impostazioni.php?delete-admin=<?php echo $admin['id'] ?>">
									</a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			<?php endif ?>
		</div>
	</div>


</body>

</html>