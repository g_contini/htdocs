<?php
// Variabili del post
$post_id = 0;
$isEditingPost = false;
$published = 0;
$title = "";
$post_slug = "";
$body = "";
$featured_image = "";

// Ritorna i post dal DB
function getAllPosts()
{
	global $conn;


	$sql = "SELECT * FROM posts";

	$result = mysqli_query($conn, $sql);
	$posts = mysqli_fetch_all($result, MYSQLI_ASSOC);

	return $posts;
}


// L'utente clicca il pulsante add article
if (isset($_POST['create_post'])) {
	createPost($_POST);
}
// Se l'utente clicca modifica articolo
if (isset($_GET['edit-post'])) {
	$isEditingPost = true;
	$post_id = $_GET['edit-post'];
	editPost($post_id);
}
// Se l'utente clicca aggiorna articolo
if (isset($_POST['update_post'])) {
	updatePost($_POST);
}
// Se l'utente clicca elimina articolo
if (isset($_GET['delete-post'])) {
	$post_id = $_GET['delete-post'];
	deletePost($post_id);
}


function createPost($request_values)
{
	global $conn, $errors, $title, $featured_image, $body, $published;
	$title = esc($request_values['title']);
	$body = htmlentities(esc($request_values['body']));

	if (isset($request_values['publish'])) {
		$published = esc($request_values['publish']);
	}
	// Converte la stringa: Se il titolo è "Nuovo Articolo", ritorna "nuovo-articolo"
	$post_slug = makeSlug($title);
	// Controlla il form
	if (empty($title)) {
		array_push($errors, "Post title is required");
	}
	if (empty($body)) {
		array_push($errors, "Post body is required");
	}


	$featured_image = $_FILES['featured_image']['name'];
	if (empty($featured_image)) {
		array_push($errors, "Featured image is required");
	}
	// Directory immagine
	$target = "../static/images/" . basename($featured_image);
	if (!move_uploaded_file($_FILES['featured_image']['tmp_name'], $target)) {
		array_push($errors, "Failed to upload image. Please check file settings for your server");
	}
	$post_check_query = "SELECT * FROM posts WHERE slug='$post_slug' LIMIT 1";
	$result = mysqli_query($conn, $post_check_query);

	if (mysqli_num_rows($result) > 0) { // Se il post esiste già
		array_push($errors, "Esiste già un post con questo titolo.");
	}
	// Crea il post se non ci sono errori
	if (count($errors) == 0) {
		$query = "INSERT INTO posts (user_id, title, slug, image, body, published, created_at, updated_at) VALUES(1, '$title', '$post_slug', '$featured_image', '$body', $published, now(), now())";
		if (mysqli_query($conn, $query)) { // Se il post è stato creato con successo
			$inserted_post_id = mysqli_insert_id($conn);
			$_SESSION['message'] = "Post created successfully";
			header('location: dashboard.php');
			exit(0);
		}
	}
}

/* * * * * * * * * * * * * * * * * * * * *
	* - Prende l'id del post come parametro
	* - Prende il post dal database
	* - e setta i campi del form per modificarli
	* * * * * * * * * * * * * * * * * * * * * */
function editPost($role_id)
{
	global $conn, $title, $post_slug, $body, $published, $isEditingPost, $post_id;
	$sql = "SELECT * FROM posts WHERE id=$role_id LIMIT 1";
	$result = mysqli_query($conn, $sql);
	$post = mysqli_fetch_assoc($result);
	// Setta i campi del form
	$title = $post['title'];
	$body = $post['body'];
	$published = $post['published'];
}

function updatePost($request_values)
{
	global $conn, $errors, $post_id, $title, $featured_image, $topic_id, $body, $published;

	$title = esc($request_values['title']);
	$body = esc($request_values['body']);
	$post_id = esc($request_values['post_id']);
	if (isset($request_values['topic_id'])) {
		$topic_id = esc($request_values['topic_id']);
	}
	if (isset($request_values['publish'])) {
		$published = esc($request_values['publish']);
	}
	$post_slug = makeSlug($title);

	if (empty($title)) {
		array_push($errors, "Post title is required");
	}
	if (empty($body)) {
		array_push($errors, "Post body is required");
	}
	// Se viene inserita una nuova immagine
	if (file_exists($_FILES['featured_image']['name']) || is_uploaded_file($_FILES['featured_image']['tmp_name'])) {
		// Prendo il nome dell'immagine
		$featured_image = $_FILES['featured_image']['name'];
		// Directory del file immagine
		$target = "../static/images/" . basename($featured_image);
		if (!move_uploaded_file($_FILES['featured_image']['tmp_name'], $target)) {
			array_push($errors, "Failed to upload image. Please check file settings for your server");
		}
	} else {
		$featured_image = null;
	}


	if (count($errors) == 0 && !is_null($featured_image)) {


		$query = "UPDATE posts SET title='$title', slug='$post_slug', views=0, image='$featured_image', body='$body', published=$published, updated_at=now() WHERE id=$post_id";
		update($conn, $query);
	} else {
		$query = "UPDATE posts SET title='$title', slug='$post_slug', views=0, body='$body', published=$published, updated_at=now() WHERE id=$post_id";
		update($conn, $query);
	}
}

function update($conn, $query) {
	mysqli_query($conn, $query);
	$_SESSION['message'] = "Post updated successfully";
	header('location: dashboard.php');
	exit(0);
}
// Cancella il post
function deletePost($post_id)
{
	global $conn;
	$sql = "DELETE FROM posts WHERE id=$post_id";
	if (mysqli_query($conn, $sql)) {
		$_SESSION['message'] = "Post cancellato con successo";
		header("location: posts.php");
		exit(0);
	}
}


