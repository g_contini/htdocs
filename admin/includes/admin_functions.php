<?php 
// Variabili admin
$admin_id = 0;
$isEditingUser = false;
$username = "";
$role = "";
$email = "";

// Variabile generale
$errors = [];


// Modifica dati admin
if (isset($_GET['edit-admin'])) {
	$isEditingUser = true;
	$admin_id = $_GET['edit-admin'];
	editAdmin($admin_id);
}
// Se l'utente clicca sul pulsante aggiorna
if (isset($_POST['update_admin'])) {
	updateAdmin($_POST);
}
// Se l'utente clicca su elimina admin
if (isset($_GET['delete-admin'])) {
	$admin_id = $_GET['delete-admin'];
	deleteAdmin($admin_id);
}





// Prende come parametro l'id dell'admin

function editAdmin($admin_id)
{
	global $conn, $username, $admin_id, $email;

	$sql = "SELECT * FROM users WHERE id=$admin_id LIMIT 1";
	$result = mysqli_query($conn, $sql);
	$admin = mysqli_fetch_assoc($result);

	// Imposta i valori del form ($username and $email) per essere aggiornati
	$username = $admin['username'];
	$email = $admin['email'];
}

// Richiesta di aggiornamento
function updateAdmin($request_values){
	global $conn, $errors, $role, $username,$admin_id, $email;
	// Preleva l'id dell'admin
	$admin_id = $request_values['admin_id'];
	


	$username = esc($request_values['username']);
	$email = esc($request_values['email']);
	$password = esc($request_values['password']);
	$passwordConfirmation = esc($request_values['passwordConfirmation']);
	
	
	if (count($errors) == 0) {
		//Hash md5
		$password = md5($password);

		$query = "UPDATE users SET username='$username', email='$email', password='$password' WHERE id=$admin_id";
		mysqli_query($conn, $query);

		$_SESSION['message'] = "Informazioni aggiornate con successo";
		header('location: informazioni.php');
		exit(0);
	}
}
// Elimina utente
function deleteAdmin($admin_id) {
	global $conn;
	$sql = "DELETE FROM users WHERE id=$admin_id";
	if (mysqli_query($conn, $sql)) {
		$_SESSION['message'] = "Utente cancellato con successo";
		header("location: users.php");
		exit(0);
	}
}


// Prevenire SQL injection
function esc(String $value){
	
	global $conn;
	// Rimuovi spazi vuoti
	$val = trim($value); 
	$val = mysqli_real_escape_string($conn, $value);
	return $val;
}
// Riceve una stringa del tipo 'Nuovo Post'
// w ritorna 'nuovo-post'
function makeSlug(String $string){
	$string = strtolower($string);
	$slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
	return $slug;
}
function getAdminUsers(){
	global $conn, $roles;
	$sql = "SELECT * FROM users WHERE role IS NOT NULL";
	$result = mysqli_query($conn, $sql);
	$users = mysqli_fetch_all($result, MYSQLI_ASSOC);

	return $users;
}

?>