<?php require_once('../config.php') ?>
<?php require_once( ROOT_PATH . '/includes/checkauth.php') ?>
<?php require_once( ROOT_PATH . '/includes/public_functions.php') ?>

<!--Ritorna i post dal database---->
<?php $posts = getPublishedPosts(); ?>

<?php require_once(ROOT_PATH . '/includes/head_section.php') ?>

<title> LifeBlog | Admin </title>


<?php require_once(ROOT_PATH . '/admin/navbar_admin.php') ?>
<section class="article md-5">
    <div class="container  pt-2 mt-5">
        <div class="row mt-4">
            <?php foreach ($posts as $post): ?>
                <div class="card col-md-4 p-2">
                    <img src="<?php echo BASE_URL . '/static/images/' . $post['image']; ?>" class="card-image" alt="" style="max-height: 200px">

                    <div class="card-body">
                        <h4 class="card-title"><?php echo $post['title'] ?></h4>

							<p class="card-text"></p>
							<!-- Invia alla pagina create_post.php l'id del post stesso -->
                        <a type="submit" href="create_post.php?edit-post=<?php echo $post['id'] ?>"
                           class="btn btn-outline-success" name="edit_post">Modifica Articolo</a>

                    </div>
                    <div class="card-footer text-muted d-flex justify-content-between bg-transparent border-top-0">
                        <div class="views"><?php echo $post['created_at'] ?>
                        </div>
                        <div class="stats">

                        </div>

					</div>
				</div>
			<?php endforeach?>
        </div>
    </div>
</section>
