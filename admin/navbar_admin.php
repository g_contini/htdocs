

<nav class="navbar fixed-top  navbar-expand-lg navbar-light  bg-admin-section static-top">


    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <h2>MY COMMUNITY</h2>
            <a class="rounded" style="background-color: #cbeea8">admin</a>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link <?php if ($page == 'home') {
                        echo 'active';
                    } ?>" href="/index.php">Home
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link <?php if ($page == 'create_post') {
                        echo 'active';
                    } ?>" href="/admin/create_post.php">
                        <i class="fas fa-plus"></i> Add article
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($page == 'impostazioni') {
                        echo 'active';
                    } ?>" href="/admin/impostazioni.php">Impostazioni
                    </a>
                </li>
                <?php

                if (isset($_POST['logout_btn'])) {
                    $_SESSION['loggedIn'] = 'no';
                    header('location: ' . BASE_URL . 'index.php');
                    exit(0);
                }

                ?>
                <li class="nav-item ">
                    <form action="" method="post">
                        <button class="nav-link mynav-link " href="index.php" type="submit" name="logout_btn">Logout
                        </button>
                    </form>

                </li>

            </ul>
        </div>
    </div>
</nav>