<?php require_once('../config.php') ?>
<?php require_once( ROOT_PATH . '/includes/checkauth.php') ?>
<?php include(ROOT_PATH . '/admin/includes/admin_functions.php'); ?>
<?php include(ROOT_PATH . '/admin/includes/post_functions.php'); ?>
<?php require_once(ROOT_PATH . '/includes/head_section.php') ?>
<?php $page='create_post';require_once(ROOT_PATH . '/admin/navbar_admin.php') ?>







<div class="container cont-bg" style="margin-top: 100px ; margin-bottom:200px">
    <div class="row ">
        <h1>Qui Puoi Aggiungere o modificare il tuo articolo</h1>
    </div>
    <div class="row" style="margin-top: 100px">
        <div class="col-12">
            <form method="post" enctype="multipart/form-data" action="<?php echo BASE_URL . '/admin/create_post.php'; ?>">
                <?php include(ROOT_PATH . '/includes/errors.php') ?>

                <?php if ($isEditingPost === true) : ?>
                    <input type="hidden" name="post_id" value="<?php echo $post_id; ?>">
                <?php endif ?>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Titolo</label>
                    <input type="text" class="form-control" name="title" placeholder="Titolo" value="<?php echo $title; ?>" ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput2">Immagine</label>
                    <input type="file" class="form-control" name="featured_image" placeholder="Nessun file selezionato">
                </div>


                <div class="form-group">

                    <textarea class="form-control" name="body" id="body" rows="10" cols="30">
                        <?php echo $body; ?></textarea>
                </div>

              

                <div class="form-check">
                    <input style="display: none" type="checkbox" class="form-check-input" id="exampleCheck1" value="1" name="publish" checked>
                    <label style="display: none" class="form-check-label" for="exampleCheck1">Pubblica</label>
                </div>
                <?php if ($isEditingPost === true) : ?>
                    <button type="submit" href="#" class="btn btn-lg btn-success" name="update_post">Aggiorna</button>
                <?php else : ?>
                    <button type="submit" href="#" class="btn btn-lg btn-success" name="create_post">Aggiungi</button>
                <?php endif ?>
            </form>
        </div>
    </div>

</div>




<script>
    CKEDITOR.replace('body');
</script>