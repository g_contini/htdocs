<!-- include il file config.php -->
<?php require_once('config.php') ?>
<?php require_once( ROOT_PATH . '/includes/public_functions.php') ?>

<!--Ritorna i post che risultano pubblicati---->
<?php $posts = getPublishedPosts(); ?>

<?php require_once(ROOT_PATH . '/includes/head_section.php') ?>


<script>
$(window).scroll(function(){
	$('nav').toggleClass('scrolled', $(this).scrollTop() > 360);
});
</script>

<script>
    $(window).scroll(function(){
        if($(this).scrollTop() > 360){
            $('.abc').addClass('ncol');}
        else{
            $('.abc').removeClass('ncol');}
    });
</script>

</head>

<body>

<!-- navbar -->
<?php $page = 'home';
include(ROOT_PATH . '/includes/navbar.php') ?>
<!-- // navbar -->


    <section class="mainsection">
        <div class="container  pt-5 mt-5">
            <div class="row">
                <div class="col-lg-5 col-sm-12">
                    <div class="cont" style="margin-bottom:30px ">
                        <h1 class="tit">My Community Blog</h1>
                    </div>
                    <p class="title-main">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iure quaerat ullam quae ipsum iusto eligendi ducimus voluptates praesentium
                        delectus aspernatur sequi earum nesciunt fuga sapiente voluptatem vel, ab blanditiis. Officiis, exercitationem ut rem asperiores harum
                        quasi doloremque earum possimus? Eius repudiandae veritatis reiciendis commodi praesentium numquam aliquam mollitia officiis
                        corporis!</p>

                    <a href="#" class="btn btn-lg btn-success mybtn">Visita il nostro blog</a>
                </div>
            </div>
        </div>
    </section>


    <section class="article mt-4">
        <div class="container">
            <div class="row">
                <!--Scorre i post pubblicati e memorizza nella variabile $post  -->
                <?php foreach ($posts as $post): ?>
                    <div class="card col-md-4 p-2">
                           <img src="<?php echo BASE_URL . '/static/images/' . $post['image']; ?>" class="card-image" alt="" style="max-height: 200px">

                        <div class="card-body">
                            <h4 class="card-title"><?php echo $post['title'] ?></h4>

							<p class="card-text"></p>
							<!-- Invia al file "single_post.php" il titolo del post -->
							<a href="single_post.php?post-slug=<?php echo $post['slug'] ?>" class="btn btn-outline-success">Read article</a>
                        </div>
                        <div class="card-footer text-muted d-flex justify-content-between bg-transparent border-top-0">
                            <div class="views"><?php echo $post['created_at'] ?>
                            </div>
                            <div class="stats">

                            </div>

                        </div> 
                    </div>
                <?php endforeach ?>
            </div>
        </div>

    </section>


    <!-- footer -->
    <?php include(ROOT_PATH . '/includes/footer.php') ?>
