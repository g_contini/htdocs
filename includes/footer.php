<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css"/>
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css"/>
<nav class="bottom shuffle shuffled mt-5">
  <section class="bottom_col bottom_col--intro background--active">
    <div class="bottom_logo">
        <h1 style="font-size:50px">MY COMMUNITY</h1>
        <p class="bottom_slogan">"404 slogan not found"</p>
    </div>
  </section>
    <section class="visit_style background--active heigt-20p">
        <div class="bottom_col3-wrap">
            <h4 class="bottom_col3-title">Visit</h4>
            <a class="bottom_address"
               href="https://www.google.com/maps/place/Politecnico+di+Bari/@41.1090409,16.8761605,17z/data=!3m1!4b1!4m5!3m4!1s0x1347e84f6b3975e1:0xb00d07e6c3cc2e4c!8m2!3d41.1090409!4d16.8783492"
               target="_blank">
                <span class="bottom_address-row">Politecnico di Bari</span>
                <span class="bottom_address-row">Via Edoardo Orabona</span>
                <span class="bottom_address-row">70126 Bari BA</span>
            </a>
        </div>
    </section>
    <section class="social_style background--active heigt-20p">
        <div class="bottom_col5-wrap">
            <h4 class="bottom_col5-title">Social</h4>
            <div id="shareIconsCount"></div>
            <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
            <script src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
            <script type="text/javascript">
                $("#shareIconsCount").jsSocials({
                    url: "https://mycommunity.altervista.org",
                    text: "My Community Blog",
                    showCount: true,
                    showLabel: false,
                    shares: [
                        {share: "twitter", via: "artem_tabalin", hashtags: "mycommunity"},
                        "facebook",
                        "googleplus",
                        
                        "pinterest",
                        "stumbleupon",
                        "whatsapp"
                    ]
                });
            </script>
        </div>
    </section>
    <a class="bottom_legal">© 2020 My Community. All Rights Reserved.</a>
</nav>
</body>
</html>