<?php 
/* Ritorna tutti i post pubblicati dal database */
function getPublishedPosts() {
	// utilizza la variabile globale per la connessione al database
	global $conn;
	$sql = "SELECT * FROM posts WHERE published=true";
	$result = mysqli_query($conn, $sql);
	// memorizza tutti i post in un array 
	$posts = mysqli_fetch_all($result, MYSQLI_ASSOC);
	return $posts;
}

/* Ritorna il Post in base al titolo */
function getPost($slug){
	global $conn;
	
	$post_slug = $_GET['post-slug'];
	$sql = "SELECT * FROM posts WHERE slug='$post_slug' AND published=true";
	$result = mysqli_query($conn, $sql);

	// memorizza la query in un array.
	$post = mysqli_fetch_assoc($result);
	
	return $post;
}




?>