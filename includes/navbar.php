<!-- Navigation -->
<nav class="navbar fixed-top navbar-expand-lg navbar-default static-top">

    <div class="container-fluid">
        <a class="navbar-brand abc" href="#">
            <h2>MY COMMUNITY</h2>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">
            <i class="fas fa-bars" style="color: #fff; font-size:28px;"></i>
            </span>
        </button>
        <!--definiamo la navbar con i collegamenti alle pagine del blog-->
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item ">
                    <!--usiamo un if con php per rendere attivo il button della pagina che è in visualizzazione-->
                    <a class=" abc nav-link <?php if ($page == 'home') {
                        echo 'active';
                    } ?>" href="index.php">Home
                    </a>
                </li>
                <li class="nav-item">
                    <a class="abc nav-link <?php if ($page == 'about') {
                        echo 'active';
                    } ?>" href="about.php">About
                    </a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link<?php /*if($page=='services'){echo'active';}*/ ?>" href="#">Services
                    </a>
                </li>-->
                <li class="nav-item">
                    <a class="nav-link abc <?php if ($page == 'contact') {
                        echo 'active';
                    } ?>" href="contact.php">Contact
                    </a>
                </li>
                <li class="nav-item">
                    <a href="login.php" class="nav-link abc"><i class="fas fa-lock"></i></a>

                </li>
            </ul>
        </div>
    </div>
</nav>
