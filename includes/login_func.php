<?php
// Dichiarazione variabili
$username = "";
$email    = "";
$errors = array();


// Login
if (isset($_POST['login_btn'])) {
	$username = esc($_POST['username']);
	$password = esc($_POST['password']);
	// Controllo username e password

	if (empty($username) or empty($password)) {
		array_push($errors, "Username e Password necessari");
	}
	/*if (empty($password)) {
		array_push($errors, "Password richiesta");
	}*/
	if (empty($errors)) {
		$password = md5($password); // Funzione di hash md5


		$sql = "SELECT * FROM users WHERE username='$username' and password='$password' LIMIT 1";

		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			// Preleva l'id dell'utente loggato
			$reg_user_id = mysqli_fetch_assoc($result)['id'];

			// Variabile lo stato della sessione utente 
			$_SESSION['loggedIn'] = 'yes';
			// Reindirizza alla area dashboard
			header('location: ' . BASE_URL . '/admin/dashboard.php');
			exit(0);
		} else {
			array_push($errors, 'Credenziali errate');
		}
	}
}
if (isset($_POST['logout_btn'])) {
	$_SESSION['loggedIn'] = 'no';
	header('location: ' . BASE_URL . '/index.php');
	exit(0);
}

function esc($value)
{

	global $conn;

	$val = trim($value); // Rimuove gli spazi bianchi
	$val = mysqli_real_escape_string($conn, $value); //rimozione caratteri speciali

	return $val;
}
// Utente tramite id
function getUserById($id)
{
	global $conn;
	$sql = "SELECT * FROM users WHERE id=$id LIMIT 1";

	$result = mysqli_query($conn, $sql);
	$user = mysqli_fetch_assoc($result);

	// ritorna l'utente in un array
	// ['id'=>1 'username' => 'admin', 'email'=>'admin@admin.com', 'password'=> '1234']
	return $user;
}
