<?php include('config.php'); ?>


<?php include(ROOT_PATH . '/includes/head_section.php'); ?>
<title>Admin | Manage </title>

<script>
$(window).scroll(function(){
	$('nav').toggleClass('scrolled', $(this).scrollTop() > 360);
});
</script>

<script>
    $(window).scroll(function(){
        if($(this).scrollTop() > 360){
            $('.abc').addClass('ncol');}
        else{
            $('.abc').removeClass('ncol');}
    });
</script>

</head>


<body>
<!-- admin navbar -->
<?php $page = 'contact';
include(ROOT_PATH . '/includes/navbar.php') ?>

<section class="circa mt-5 pt-5">
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="contact-info">
                <img src="https://image.ibb.co/kUASdV/contact-image.png" alt="image"/>
                <h2>Contattaci</h2>
                <h4>Non esitare a contattarci!</h4>
            </div>
        </div>
		<div class="col-md-9">
			
				<form class="contact-form" method="post" action="/includes/form_func.php" >
					<div class="form-group">
					  <label class="control-label col-sm-2" for="fname">Nome:</label>
					  <div class="col-sm-10">          
						<input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="fname">
					  </div>
					</div>
					<div class="form-group">
					  <label class="control-label col-sm-2" for="lname">Cognome:</label>
					  <div class="col-sm-10">          
						<input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lname">
					  </div>
					</div>
					<div class="form-group">
					  <label class="control-label col-sm-2" for="email">Email:</label>
					  <div class="col-sm-10">
						<input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
					  </div>
					</div>
					<div class="form-group">
					  <label class="control-label col-sm-2" for="message">Commento:</label>
					  <div class="col-sm-10">
						<textarea class="form-control" rows="5" id="message" name="message"></textarea>
					  </div>
					</div>
					<div class="form-group">        
					  <div class="col-sm-offset-2 col-sm-10">
                        <button style="color: white; background-color:#53b96a" type="submit" name="submit"  
                        class="btn btn-lg btn-success mybtn">Submit</button>
					  </div>
					</div>
</form>
			
		</div>
	</div>
</div>
</section>

<?php include(ROOT_PATH . '/includes/footer.php') ?>